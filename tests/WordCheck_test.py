from checks import WordCheck
import pytest

@pytest.mark.word
def test_isWord():
    # get check result
    ok, msg = WordCheck.isWord()
    # Test logic
    assert ok, msg
