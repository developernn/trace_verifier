from checks import MathCheck
import pytest

@pytest.mark.math
def test_multiply():
    # get check result
    ok, msg = MathCheck.multiply(5, 5)
    # Test logic
    assert ok, msg
